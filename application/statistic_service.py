import logging
import os
import sys
import json

from flask import Flask, send_from_directory, request, Response
from flask_cors import CORS
from marshmallow import validate
from sqlalchemy.exc import ProgrammingError

from util.app_configuration import AppConfiguration
from data_store.database_connector import DatabaseConnector
from network.edp_request_sender import EdpRequestSender
from network.upload_api_connector import UploadApiConnector
from xlsx.xlsx_handler import XlsxHandler
from web.json_handler import JsonHandler
from util.identifier import Identifier
from request_scheduler import RequestScheduler

from flask_restful import Resource, Api
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from flask_apispec.views import MethodResource
from flask_apispec import doc, use_kwargs
import marshmallow as ma
from datetime import date


# Read system configuration
conf = AppConfiguration()
conf.read_config("../application_config.ini")

# Initialise Logging-System (-> stdout / stderr)
logging.basicConfig(level=logging.DEBUG, stream=sys.stdout,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger("Main")
log.info("Configuration and Logger initialised.")

if os.environ.get("APISPEC_SWAGGER_UI_URL"):
    APISPEC_SWAGGER_UI_URL = os.environ.get("APISPEC_SWAGGER_UI_URL")
else:
    APISPEC_SWAGGER_UI_URL = '/swagger-ui/'
log.info("APISPEC_SWAGGER_UI_URL=" + APISPEC_SWAGGER_UI_URL)

if os.environ.get("APISPEC_SWAGGER_URL"):
    APISPEC_SWAGGER_URL = os.environ.get("APISPEC_SWAGGER_URL")
else:
    APISPEC_SWAGGER_URL = '/swagger/'
log.info("APISPEC_SWAGGER_URL=" + APISPEC_SWAGGER_URL)

if os.environ.get("ALTERNATIVE_ROOT_PATH"):
    ALTERNATIVE_ROOT_PATH = os.environ.get("ALTERNATIVE_ROOT_PATH")
else:
    ALTERNATIVE_ROOT_PATH = '/api/hub/statistics'
log.info("ALTERNATIVE_ROOT_PATH=" + ALTERNATIVE_ROOT_PATH)


class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    :param app: the WSGI application
    '''

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = ALTERNATIVE_ROOT_PATH
        environ['SCRIPT_NAME'] = script_name
        path_info = environ['PATH_INFO']
        if path_info.startswith(script_name):
            environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)


# Flask app instance initiated
version = 'v1.3'
app = Flask(__name__)
api = Api(app)  # Flask restful wraps Flask app around it.
app.config.update({
    'APISPEC_SPEC': APISpec(
        title='hub statistics API',
        version=version,
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0'
    ),
    'APISPEC_SWAGGER_URL': APISPEC_SWAGGER_URL,  # URI to access API Doc JSON
    'APISPEC_SWAGGER_UI_URL': APISPEC_SWAGGER_UI_URL  # URI to access UI of API Doc
})
app.wsgi_app = ReverseProxied(app.wsgi_app)
docs = FlaskApiSpec(app)

CORS(app)
# CORS(app, resources={r"/*": {"origins": r"*fokus.fraunhofer.de"}})
log.info('Flask App ' + version + ' initialised.')

# Initialse database
log.info("Initialse database...........")
db = DatabaseConnector()
db.create_database()
log.info("Database initialised.")

# Initialise request scheduler
scheduler = RequestScheduler()
scheduler.start()


@app.route('/')
def hello():
    # return doc of swagger ui
    return docs.swagger_ui()


class Xlsx(MethodResource, Resource):
    """Returns a XLSX file. All params are optional."""

    # @ns.doc('get_xlsx')
    @doc(description="startdate and enddate should be in " + str(date.today()) + " format", tags=['Xlsx'],
         params={'interval': {'description': 'The interval'}, 'enddate': {'description': 'The enddate'},
                 'startdate': {'description': 'The startdate'},
                 }, )
    @use_kwargs({'interval': ma.fields.Int(required=True, validate=validate.Range(min=1))}, location="query")
    @use_kwargs({'enddate': ma.fields.Date(required=True)}, location="query")
    @use_kwargs({'startdate': ma.fields.Date(required=True)}, location="query")
    def get(self, interval, enddate, startdate):
        """Get a xlsx"""

        ip = request.remote_addr
        start_date = startdate.strftime('%Y-%m-%d')
        log.info("get Xlsx start_date:" + start_date)
        end_date = enddate.strftime('%Y-%m-%d')
        log.info("get Xlsx end_date:" + end_date)
        log.info("get Xlsx interval:" + str(interval))
        try:
            interval = int(interval)
        except TypeError:
            interval = 1
        xlsx = XlsxHandler()
        filename = xlsx.create_xlsx_file(ip, start_date, end_date, interval)
        return send_file_to_clint(filename)

    # @ns.doc('delete_xlsx')
    @doc(description="Deletes generated xlsx files.", tags=['Xlsx'])
    def delete(self):
        """Deletes generated xlsx files."""
        xlsx = XlsxHandler()
        xlsx.delete_xlsx()
        return Response('Deletion of all xlsx files successful.', 200, mimetype="text/plain")


api.add_resource(Xlsx, '/xlsx')
docs.register(Xlsx)


# @ns.route('/data')
class ListData(MethodResource, Resource):

    # @ns.doc('get_data')
    @doc(description="Retruns an overview about the available dates for further requests.", tags=['Data'])
    def get(self):
        """Retruns an overview about the available dates for further requests."""
        return Response(json.dumps(_build_json_for_unkown_identifier(200, "See list of available identifiers.")), 200,
                        mimetype="application/json")


api.add_resource(ListData, '/data')
docs.register(ListData)


# @ns.route('/data/<string:identifier>')
# @ns.param('identifier', 'The identifier')
# identifier is required as path
class GetData(MethodResource, Resource):
    @doc(description="Retruns specific dates for identifier.", tags=['Data'],
         params={'identifier': {'description': 'The identifier of specific data.'}})
    # @doc(description="Retruns specific dates for identifier.", tags=['Data'])
    # @use_kwargs({'identifier': ma.fields.Str(required=True)}, location="path")
    def get(self, identifier: str):
        """Retruns specific dates for identifier."""
        log.info("GetData for identifier=" + identifier)
        if identifier not in Identifier.get_all_identifier():
            return Response(json.dumps(_build_json_for_unkown_identifier(400, "Identifier unknown.")), 400,
                            mimetype="application/json")
        json_handler = JsonHandler()
        try:
            if identifier in Identifier.get_all_identifier_keywords():
                if bool(request.args.get("list")):
                    json_data = json_handler.create_json(identifier)
                    status_code = 200
                else:
                    json_data = json_handler.create_json(identifier)
                    status_code = 200
        except (ProgrammingError, IndexError):
            json_data = _build_error_json()
            status_code = 404
        return Response(json_data, status_code, mimetype="application/json")


api.add_resource(GetData, '/data/<string:identifier>')
docs.register(GetData)


# @ns.route('/grab-data')
class Harvesting(MethodResource, Resource):
    """Initialize the data harvesting task"""

    @doc(description="Initialize the data harvesting.", tags=['Data'])
    # @ns.doc('grab_data')
    def get(self):
        """Initialize the data harvesting."""
        start_edp_request()
        return Response("Manually request to EDP started.", 200, mimetype="text/plain")


api.add_resource(Harvesting, '/grab-data')
docs.register(Harvesting)


# @ns.route('/test')
class Test(MethodResource, Resource):
    """Test case"""

    # @ns.doc('test_case')
    @doc(description="Execute a test case for data upload.", tags=['Test'])
    def put(self):
        """Execute a test case for data upload."""
        upload = UploadApiConnector()
        filename = upload.create_xlsx_file()
        status = upload.prepare_upload(filename)
        log.debug("Prepare upload finished with status {}".format(status))
        if status == 200:
            log.info("Data upload prepared successful.")
            upload_status = upload.send_xlsx_to_hub(filename)
            if upload_status == 200:
                log.info("Data upload successful.")
                upload.add_distribution_to_dataset(filename)
                return Response("Upload successfull", 200, mimetype="text/plain")
            else:
                return Response("Upload failed", 500, mimetype="text/plain")
        else:
            return Response("Upload prepare failed with status code %s" % status, 500, mimetype="text/plain")


api.add_resource(Test, '/test')
docs.register(Test)


def send_file_to_clint(filename):
    return send_from_directory(conf.resource_path, filename, as_attachment=True)


def _build_json_for_unkown_identifier(statuscode, message) -> dict:
    identifier_list = []
    for item in Identifier.get_all_identifier().keys():
        identifier_list.append(item)
    return {
        "statusCode": statuscode,
        "message": message,
        "available_identifier": identifier_list
    }


def _build_error_json() -> json:
    return json.dumps({"statusCode": 404, "message": "Data not available."})


def start_edp_request():
    request_sender_thread = EdpRequestSender()
    request_sender_thread.start()
